Jason Berger 
============

.. |github| image:: ../assets/icons/github.svg 
    :target: https://github.com/berge472
    :alt: Github
    :width: 25px
.. |linkedin| image:: ../assets/icons/linkedin.svg
    :target: https://www.linkedin.com/in/jasonberger472/
    :alt: Linkedin
    :width: 25px
.. |gitlab| image:: ../assets/icons/gitlab.png
    :target: https://gitlab.com/JasonBerger
    :alt: Gitlab
    :width: 25px
.. |email| image:: ../assets/icons/envelope-solid.svg
    :target: mailto:Berge472@gmail.com
    :alt: Email
    :width: 25px
.. |portfolio| image:: ../assets/icons/layer-group-solid.svg
    :target: https://jasonberger.gitlab.io/portfolio/pages/resume
    :alt: Portfolio
    :width: 25px
.. |spaces| raw:: html

   &nbsp;&nbsp;

.. |page_break| raw:: html
    
   <p style="page-break-after: always;">&nbsp;</p>



.. | |email| Berge472@gmail.com
.. | |github| `Github <https://github.com/berge472>`_ 
.. | |gitlab| `Gitlab <https://gitlab.com/JasonBerger>`_
.. | |linkedin| `Linkedin <https://www.linkedin.com/in/jasonberger472/>`_
.. | |portfolio| `Portfolio <https://jasonberger.gitlab.io/portfolio/pages/resume>`_

|email| |spaces| |github| |spaces| |gitlab| |spaces| |linkedin| |spaces| |portfolio|

----

Education
---------

**Western Governors University**:
    **Bachelors of Science** Software Development


Certifications: 
    - CIW Site Development Associate
    - CIW Advanced HTML5 & CSS3 Specialist
    - CompTIA A+ ce

----

Summary 
-------

Fast independent learner with experience across a wide range of technologies and disciplines. Focus on emebedded software but comfortable working with hardware and board bringup. 

    - **Languages:** C, C++, ASM, Python, Go, Java, HTML, CSS, JS/Typescript, PHP, SQL, Verilog
    - **Platforms:** STM32, STM32MP1, NXP, TI, ESP32, Atmel, nRF, Raspberry Pi, Beaglebone, Bare Metal, FreeRTOS, Linux
    - **DevOps:** CI/CD, Docker, Jenkins, Gitlab, Github, Bitbucket
    - **Build systems:** Make, CMake, Yocto, Buildroot
    - **Tools:** Oscilliscope, Logic Analyzer, SDR/gnuRadio, packet sniffers, wireshark, 3D Printing, Laser cutting, CNC, Soldering
    - **Schematic/PCB:** KiCad, Eagle, Altium 

----

|page_break|

Experience
----------


Cromulence Melbourne, FL
~~~~~~~~~~~~~~~~~~~~~~~~

**Sr. Software Engineer** Feb 2024 - Present 



Up-Rev Melbourne, FL
~~~~~~~~~~~~~~~~~~~~

| **Software Engineering Manager** Aug 2021 - Feb 2024 
| **Embedded Software Engineer** Aug 2017 - Feb 2024
| **Software Contractor** Nov 2014 - Aug 2017

    - Embedded software development for prototypes and production 
    - Reverse engineering
    - Mobile app development
    - Web development
    - DevOps (CI/CD, Docker, Jenkins, Gitlab, Github, Bitbucket)
    - Team management and planning 
    - Estimation and quoting for large scale projects

.. 
    20/80 split between CI and management tasking. Management tasks include customer facing design and planning, team management, and estimation. I also manage the DevOps for the team including CI/CD scripting, Development container configuration, and server management.

    In my CI role I am responsible for application level embedded code as well as low level device driver development. Outside of embedded/firmware development I also provide support for ansillary components to projects such as backend/front end web development.

Jaycon Systems Melbourne, FL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Embedded Systems Engineer** July 2014 - Nov 2014

    - Embedded software development
    - Schematic capture and PCB layout
    - Application Consulting



CO2Meter.com Ormond Beach, FL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Embedded Systems Engineer** Sept 2012 - July 2014

    - Designed custom embedded platforms
    - Built custom solutions for large volume customers 
    - Wrote technical manuals and application notes for products
    - Wrote firmware for custom embedded platforms
    - Wrote technical documentation for imported components 
    - Designed desktop software for products 
    - Supported troubleshooting and repair of products

|page_break|

Geekify Inc. Boulder, CO 
~~~~~~~~~~~~~~~~~~~~~~~~

**Research and Development Engineer** April 2012 - Sept 2012

    - Took ideas from concept to prototype
    - Schematic/layout/Assembly 
    - Firmware development (primarily AVR)
    - Component selection and procurement



Occam Robotics Boulder, CO
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Prototyping Tech** May 2012 - Sept 2012

    - CAD design for 3d parts
    - Rapid prototyping (3D printing, laser cutting, CNC)

|