BLE Profile Utility [2020]
==========================

In 2020 I worked on a number of projects that requied custom GATT profiles, and decided to write a tool to help me with this. This tool is a simple command line tool that was added to the `mrtutils` package. It allows the user to define a custom GATT profile in YAML and generate the required C code as well as a single page web application to interact with the device.


Example profile definition
--------------------------

The defintion file for the profile uses an easy to read YAML format and has a lot of time saving features like the ability to include standard profiles and services by providing a SIG uri. 

.. code:: yaml 

    ---
    name: sample
    author: Jason Berger
    created: 02/20/2020
    desc: GATT profile example
    prefix: tp


    services: #list multiple services in file to create full profile

    ##############################################################
    #                      Device Service                        #
    #                                                            #
    #  Shows example of using Bluetoot SIG define Services       #
    ##############################################################
    - Device:
        uri: org.bluetooth.service.device_information #User URI of bluetooth sig standard service. For a list of all standard services visit https://www.bluetooth.com/specifications/gatt/services
        prefix: dvc
        chars:  #list out uris of 'optional' desired chars for bluetooth SIG services
            - {uri: org.bluetooth.characteristic.manufacturer_name_string , default: Up-Rev} #Set a default value
            - {uri: org.bluetooth.characteristic.serial_number_string}
            - {uri: org.bluetooth.characteristic.hardware_revision_string}
            - {uri: org.bluetooth.characteristic.firmware_revision_string, desc: Firmware revision} #You can override defaults from Bluetooth SIG (name,desc, perm, etc..)

    ##############################################################
    #                      Battery Service                       #
    #                                                            #
    #  Shows example of inline declaration for standard serivce  #
    ##############################################################
    - Battery: {uri: org.bluetooth.service.battery_service}
        #if a prefix isnt specified it will create one using the first 3 characters of the name.
        #no need to list chars, because there is only one for the battery service and it is mandatory per the SIG spec

    ##############################################################
    #                        Sprinkler Servive                   #
    #                                                            #
    # Show example of creating a custom service to control an    #
    # Automated sprinler system                                  #
    #                                                            #
    #  - Controls 6 valves and pump for sprinklers               #
    #  - Temperature sensor                                      #
    #  - 6 soil moisture sensors                                 #
    ##############################################################
    - Sprinkler:
        prefix: spr
        desc: Custom service for a sprinkler system
        uuid: 71a8-1b49-ce39-0088-6b62-c8ed-9e20-9a5b
        icon: fa-faucet # This adds an icon to the Live ICD for the service using Font-Awesome. Visit their site to view options: https://fontawesome.com/icons?d=gallery&m=free
        chars:

            - Thresh: { type: uint16, perm: RW , desc: Moisture Threshold to turn on the sprinklers} #if char uuid is blank, it will increment from previous char, or service uuid if it is the first in the service

            - Temperature: { type: uint16, perm: RN , desc: Temperature reading from sensor, unit: °f, coef: 0.01} #unit and coef have no affect on data, just how ther are displayed in the live ICD

            - Moisture: {type: uint16*6, desc: Moisture readings from all 6 zones, unit: "%" } # Create an array of 6 uint16_t values.

            - Relays:
                type: flags #flags create an array of bits which are individualy controlled
                perm: RWN   #Read Write and Notify permissions
                desc: Controls Relays for pump and valves
                vals:
                - pump: {desc: pump control}
                - valve01: valve 1 control #For convenience values can be written in this shorthand. same as '- valve01: {desc: valve 1 control}'
                - valve02: valve 2 control
                - valve03: valve 3 control
                - valve04: valve 4 control
                - valve05: valve 5 control
                - valve06: valve 6 control

            - SoilType:
                type: enum  #enums are treated as an unsigned int, but they have symbols defined and a switch case generated in the write handler
                perm: RW
                desc: Soil type for the yard
                vals:
                - Peat: Peat soil
                - Sand: Peat soil
                - Clay: Peat soil
                - TopSoil


    ##############################################################
    #                      Firmware OTA Service                  #
    ##############################################################
    - FOTA:
        desc: sercive for performing over the air updates
        uuid: 71a8-1b49-ce39-0088-6b62-c8ed-9A10-9a5b
        prefix: ota
        chars:
            - version:    { type: string,   perm: RW, desc: current Firmware version}  # uuid: 0x9A11
            - newVerion:  {type: string,   perm: RW, desc: version of new firmware being loaded}
            - data:       {type: uint8*64,  perm: RW, desc: current block of data}
            - seq:        {type: uint32,    perm: RW, desc: sequence number of current block  }
            - crc:        {type: uint32,    perm: RW, desc: crc of new firmware  }
            - status:
                type: enum
                perm: RW
                desc: status of OTA process
                vals:
                - IDLE: { desc: no ota operation taking place}
                - DOWNLOAD: { desc: Currently downloading new firmware}
                - COMPLETE: { desc: Firmware download complete. ready to update}


Single Page Web App
-------------------

The single page web application uses the Web Bluetooth API to connect to the device and interact with the GATT profile. It provides logical UI controls based on the datatype, and removes the need to write a custom app in order to interact with the device.

.. image:: ../../assets/images/live_icd.png
    :target: ../../_static/example_live_icd.html

`View Live Demo <../../_static/example_live_icd.html>`_



More Information
----------------

For more information on the `mrt-ble` package see the `Documentation <https://mrt.readthedocs.io/en/latest/pages/mrtutils/mrt-ble.html>`_

Check out the `nrf5-gatt-example example project <https://gitlab.com/uprev/public/examples/nrf5-gatt-example>`_ to see this tool in action.

Lessons Learned
---------------

This project was a great learning experience for me, and I believe it really helped our team on sevral projects. I dont know that there is much I would change as far as the tool design, but I think more logging in the web app would have been a good idea. 

I did learn a valuable lesson in relying on web content being available. Originally the tool would us the BlueTooth SIG xml schemas when importing a standard service/characteristic. But then one day the tool stopped working properly and I realized that the schema files were no longer hosted on their site. I ended up downloading the files and hosting them in a repo so we could have ownership of them moving forward. 