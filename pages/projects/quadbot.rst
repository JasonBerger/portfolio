Quadraped Robotics Kit [2012]
=============================

The goal of this project was actually to create a product, which was a quadraped robotics kit for educational purposes. I was still fairly new to embedded systems, and was really out of my depth in terms of what goes into manufacturing a product. Even though this never made it to market, I learned a lot about the process of designing a product. 


Rev 1 
-----

The first iteration was built using pieces of plexiglass that I sketched out, cut by hand, and bent using a heat gun. It had 12 servos controlled by an Arduino Uno. Because the ATMega328 does not have enough PWM channels, I used 4017 decade counters to control the signals. This video shows a demo motion sequence where it stands up, positions itself to be stable on 3 limbs, and then lifts the 4th limb so it can be used to interact with an object. 



.. youtube:: eraxWMEW-QA
    :align: center

|
|
|

Rev 2
-----

For the next iteration I designed and modeled the parts in Solidworks, then printed them on a 3d printer. At this point I had also designed custom PCBs for the project. There were 2 PCBs that would stack inside of the body of the robot. One had the ATMega328 and an FTDI IC, and the other had the power regulation and servo drivers (4017 decade counters). I believe at the time of this video, only the servo drive board was assembled which is why there is an arduino pro hanging out of the side. 

This video shows the same motion sequence as above. Aside from the obviuous aesthetic improvements, the motion is much smoother due to improvements in the code. 

.. youtube:: CVep1Rr8qAg 
    :align: center

|
|
|

Lessons Learned 
---------------

This was my first dive into CAD and 3d printing , so I would say those we my largest technical takeaways. I also got some good experience in trying to source parts and services for a product, and the real cost that goes into something like this. 

In the end I decided this would not be a feasible product largely because the cost of 12 servos strong enough to move with any weight was too high. I didnt think the market would support a hobby kit at the price pointed needed to make it profitable. But in the end I would definitely do it again because it was a fun project and I learned a lot.