Logitech Vulnerability POC [2018] 
=================================

This was some experimentation I did with known vulnerabilities in Logitech's wireless protocol. For a long time they used the nRF ESB (Enhanced ShockBurst) protocol, and the encryption was not implemented properly. I did not discover these vulnerabilities, but I did use them to create a proof of concept for a keystroke injection attack tool. 

Vulnerability
-------------

There were three characteristics of the system that contributed to the vulnerability:

1. The receiver would accept and process a packet even if the sequence number did not match the expected value. This allowed for replay attacks. 

2. The packet was encrypted by XORing the plaintext with the keystream. So if you knew the original plaintext of the packet (or section of the packet), you could maniuplate the ciphertext to produce the desired plaintext without knowing the key.

3. The data portion of the packets contained HID reports, and the HID report when all keys are released contains all zeros.


So you could capture packets, find one that contained an HID report from a key being released, and then use it as a blank canvas to create HID reports that you could send to the target machine.


Proof Of concept
----------------

Using an nRF24LU1+ USB dongle, I was able to scan for packets and detemine which ones contained HID reports. By looking at the timing and order of packets, I was able to isolate the packets that contained the release of a key. These packets were then used to inject keystrokes to the target machine. The tool I wrote had a GUI that allowed a user to select targets, find the appropriate packets, and then inject keystrokes. I wrote a bare bones parser that allowed the user to run ducky scripts from the popular USB Rubber Ducky.

One of the limitations in previous examples of this vulnerability was the attacker could send keystrokes but not easily retrieve data from the target machine. I had a few ideas to get around this: 

- Open a TCP socket back to the attacker's machine. This would allow the attacker to send itself messages/data from the target machine.
- Use a service like pastebin to store data. The attacker can create a bin and then add data to it from the target machine. 
- Use HID indications (Caps Lock, Num Lock, etc). Since the status of the keys are sent out to connected keyboards, in theory you could use this to encode data that the attack machine could read. I was not able to prove this out and moved on to the other methods because it would likely be very slow.


Results 
-------

Obviuously there are a lot of nefarious things that could be done with this tool. I had functioning scripts that could get the browser history, retrieve stored auto-fill credentials for a given URL, and get the WiFi credentials from the target machine. But the only one I used outside of a controlled environment was the one below which lets you supply the URL for a photo, and it will set it as the background on the target machine. 

.. youtube:: qDHBKuClyaE
    :align: center

|
|
|