Card Detection [2014]
=====================

In 2014 I wanted to learn more about computer vision and specifically get familiar with OpenCV. On a trip with my father I played blackjack next to a man who was legally blind and needed the dealer to anounce his cards. I had also known of a professional poker play who had to quit when he lost his vision, and was later allowed to play in the WSOP with the assistance of a his brother reading his cards to him. I thought it would be interesting to see if I could build a system that could read cards for people with vision impairments.

The plan was to prototype in linux, and then build a battery powered device that a user could plug headphones into. My vision was a small device with a camera that could be placed on the table in front of the user, and the user could lift the corners of the cards to show the camera. 

For my approach I used an IR camera with a small IR LED to illuminate the immediate area in front of the camera. This provided a clear image with good contrast. By finding the edges of the card we could locate the corner and determine the transform needed to put the card upright in the frame. Then blob detection was used to find and isolate the value and suit which would be determined with template matching. 

The proof of concept worked pretty reliably, but I ended up moving on to other projects before building the device.

.. youtube:: WkkHsNJt4pY
    :align: center

|
|

Lessons Learned 
---------------

Obviuously the biggest educational take away from this was getting familiar with OpenCV. I also learned a lot about the challenges of working with imperfect data. 