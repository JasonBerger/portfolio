GEO Location Blocking [2023]
============================

Dubbed project 'Billy Ghost', this goal of this project is to create a USB WiFi adapter that can be used to hide a users location. 

A number of software and service providers track the users location to limit access to content and services. In some cases these providers require the user to run software which verifies that the more common methods of location blocking, such as VPNs, are not being used. The more sophisticated providers will also require WiFi to be enabled and use AP Beacon Frames to verify a users location. By referencing the nearby access points with an aggregated database of known locations, the provider can determine if the users location matches what is expected. 

`Billy Ghost` defeats these measures by providing a VPN connection that is completely hidden from the host, as well as supplying beacon frames from the same aggragated database of known locations. The host has no way of detecting this because all of the countermeasures take place on the hardware, and it enumerates to the host as a basic 802.11 USB adapter. Several architectures have been considered, but the current design uses an SOC running Linux which sits between a WiFi module and a USB port. 



Step 1: Reverse Engineering
---------------------------

This is the current phase of the project. A number of readily available off-the-shelf USB WiFi adapters have been selected and are currently going through reverse engineering efforts so that the protocols can be replicated. This will allow the device to be used with existing drivers, since the need to install special software or drivers would create an easy way for service providers to detect the dvice. 


Step 2: g_wifi Development
--------------------------

The central component of the system is `g_wifi`, a Linux kernel module which enables the device to enumerate as a USB WiFi adapter. The module is based on the `g_ether` module, which is used to emulate a USB Ethernet adapter, but includes other features specific to the 802.11 specification. The module will have several configuration options including the ability to mimic different chipsets, and the ability to inject `Beacon Frames` from various sources including a static file, or a remote server.


Step 3: Portal/VPN integration 
------------------------------

The device will expose a web portal which the user can access to configure the device. The portal will allow the user to configure the VPN settings, connect to an access point, and adjust other options on the device. 


Step 4: Hardware Design
-----------------------

All of the above steps will be done in a `Proof Of Concept` phase using off-the-shelf hardware such as a Beaglebone Black. Once that stage is complete, a custom PCB will be designed. A number of interesting SOCs have been identified which can be used to create a custom board. 


Step 5: Server Image 
--------------------

The last step will be to create a custom linux image for deploying servers. The idea here is that if you want the most undetectable maskign possible, you can run your own VPN server on a remote machine that is running at the location you are trying to spoof. The server would route traffic as well as provide actual real-time beacon frames from the location. 
