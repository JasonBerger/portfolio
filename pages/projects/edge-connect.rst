Edge Debug Fixture [2019]
=========================

A common pain point for board bringup is connecting the logic analyzer to signals on the board. Even on boards large enough to provide adequate test points, the test points are often not labeled, and the user must consult the schematic to determine which test point corresponds to which signal. Then wires need to be soldered to the test points, and the wires connected to the logic analyzer. This is time consuming, error prone, and usually results in a fragile mess of wires that can easily be knocked loose.

After a particularly challenging board bring up experience, I decided took the initiative of solving this issue for our company. The goal was to create something that would give use access to a large number of signals in a way that did not consume too much board space or require additional BOM cost. 


Rev 1
-----

The solution I came up with was to create a custom PCB that would break out signals from a pci-e connector to a 0.1" header. The pci-e provides a common connector that is readily available, and the target device would not require any components, just copper traces on the edge. 


.. image:: ../../assets/images/edge-pcb.jpg 
    :align: center
    :alt: Edge PCB

|
|

Features of the board include:

    - 36 signals broken out to 0.1" header
    - A dedicated J-TAG header for programming the target device
    - A USB port and FTDI chip for convenient serial console connections 
    - A 3.3V regulator to power the target device from USB. 


.. image:: ../../assets/images/edge-assembled.jpg 
    :align: center
    :alt: Edge PCB Top

|
|

After the first batch was assembled I created a schematic symbol which looked like the board and connected it to a pci-e footprint. This allowed us to have a very clear visual diagram of which signals were connected to which pins of the board when looking at the schematic. We started adding the edge footprint to new designs and the board was very useful in streamlining development. Something I had not considered was how useful it was to be able to easily transfer a logic analyzer connection from one board to another. This was especially useful when debugging a board that was not working correctly. We could easily move the logic analyzer connection to a known working board and compare the signals. 

Rev 2 
-----

Following the success of the firs iteration, we deviced to update the design in 2022. The new design kept the same concept but added several improvements: 

    - Switched to USB Type-C connector with Power Delivery
    - Made the output of the regulator selectable
    - Added dip switches to provide pull-up/pull-down resistors on select signals
    - Added a connector for Tag-Connect cables (For boards that do not allow an edge connection)


|
|


