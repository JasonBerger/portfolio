PolyPacket [2019]
=================

PolyPacket is a tool and backend library for creating custom serial protocols. It is very similar in concept to ProtoBufs but was written in pure C with resource constrained embedded systems in mind. Users define a protocol using YAML and the tool generates the C code for the service, as well as documentation for the protocol.

The genesis of the tool was a project which started with a simple message protocol but quickly grew to include a number of different message types. The protocol was originally implemented using a simple state machine but quickly became unwieldy. PolyPacket was created to simplify the process creating, maintaining, and documenting protocols for embedded systems.

As of today the tool has been used in a number of projects and has been very successful at reducing development time. It is still actively maintained and new features are added as needed. One of the most beneficial features is the ability to open a command line interface to a device and interact with it using the protocol. This allows for rapid prototyping and testing of new features, as well as automated testing of a remote device without the need for a custom software utility. 



Example protocol definition:

.. code:: yaml 

    ---
    name: sample
    prefix: sp  #this defines the prefix used for functions and types in the code. This allows multiple protocols to be used in a project
    desc: This is a sample protocol made up to demonstrate features of the PolyPacket
    code generation tool. The idea is to have a tool that can automatically create parseable/serializable
    messaging for embedded systems

    ###########################################################################################################
    #                                   FIELDS                                                                #
    ###########################################################################################################

    fields:

    #Fields can be nested into a 'Field Group' for convenience. They will be put in the packet just like regular fields
    - header:
        - src: {type: uint16, desc: Address of node sending message }
        - dst: {type: uint16, desc: Address of node to receive message }

    - sensorA: { type: int16 ,desc: Value of Sensor A}  #Simple Fields can be defined as inline dictionares to save space

    - sensorB:
        type: int
        desc: Value of Sensor B

    - sensorName:
        type: string
        desc: Name of sensor


    ###########################################################################################################
    #                                   Packets                                                               #
    ###########################################################################################################
    packets:

    - GetData:
        desc: Message tp get data from node
        response: Data          #A response packet can be specified
        fields:
            - header

    - Data:
        desc: contains data from a sensor
        fields:
            - header
            - sensorA
            - sensorB
            - sensorName : {desc: Name of sensor sending data }   #Field descriptions can be overriden for different packets



Screenshot of the CLI:


.. image:: ../../assets/images/cli.png 

.. note:: The CLI supports transport over `UDP`, `TCP`, and `Serial` 


Plugins 
-------

A recent addition to the tool is the ability to include other protocol definitions as plugins. This keeps in line with the reusability initiative that was the catalyst for the project. 

One pre-defined plugin is the `ota` protocol which provides a messaging protocol for managing updates to the device. Along with the `PolyPacket` plugin, there is also an `OTA` utility module with provides the functionality of partitioning storage media, managing transfers, and verifying the integrity of the update. More information on how this works can be found in the `OTA module documentation <https://mrt.readthedocs.io/en/latest/moddocs/Utilities/OTA/README.html>`_


Lessons Learned 
---------------

I learned a lot working on this project. It really taught me the importance of planning and documentation, especially in modular designs. It was also probably the most challenging project I have done in terms of memory management. Because the service consists of several different modules, the ownership of memory is passed around a lot quite a bit. This involved a lot of unit testing and getting very familiar with `valgrind` to ensure I was not leaking memory. 


More Information
----------------

    - `PolyPacket documentation <https://mrt.readthedocs.io/en/latest/pages/polypacket/polypacket.html>`_
    - `PolyPacket Source Code <https://gitlab.com/uprev/public/mrt/tools/polypacket>`_
    - `PolyPacket Library <https://gitlab.com/uprev/public/mrt/modules/Utilities/PolyPacket>`_



