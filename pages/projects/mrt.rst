MrT Framework [2017]
=====================

MrT (Short for Modular Reusability and Testing) is a collection of reusable modules that can be easily integrated into new projects. Each module is designed and maintained according to guidelines and standards to keep consistency. This allows uniform implementation, documentation and testing. 

This initiative came about while working on a number of projects that required similar functionality. A lot of code was being copied between projects but not always implemented consistently, and bug fixes in one project did not always get propagated to the others. MrT is an attempt to solve this problem by providing a central repository of modules that can be used in any project.

The framework consists of a collection of modules organized by category an a toolset for managing them (`mrtutils`). Each module is a self contained repo with a readme and unit test (where applicable). The tool is a python package that retrieves the modules and adds them to a project as submodules. 

Originally there was a 'Meta' repo which included all modules as submodule, and the tool would parse the 'gitmodules' file to get a list of module URLs and theyre organizational path. But not it is hosted on Gitlab, and utilizes the Gitlab 'Group' structure which makes it easier to manage and maintain.


.. note:: The tool is not hardcoded to the specific Gitlab group and can be used with any Gitlab group or a Meta repo. So anyone is able to use the toolset with their own set of modules. 


Organization 
------------

The modules are split into three main categories: 

Platforms: 
    Modules that provide an abstraction layer for a platform (STM32, NRF, ESP32, etc). These modules provide a common interface for the platform and allow the user to write code that is platform independent.

Devices:
    Modules that contain device drivers for interacting with over devices in the system (Sensors, Actuators, etc). 

Utilities:
    Modules that provide utility functions that are not reliant on a specific platform or device. These include Fifos, hashing functions, encoder/decoders, etc.


More Information
----------------

For more information on the framework and how to use it, please see the `documentation <https://mrt.readthedocs.io/en/latest/>`_