DevOps Revamp [2021]
====================

In 2021 I stepped into an engineering management role for our software team. I made some changes to our processes in an effort to improve our development velocity and the quality of our deliverable packages. This section covers the changes I made, as well as the reasoning behind them.


Documentation 
-------------

The first major change I made was to our documentation process. Prior to this we would create a `Software Design Document` at the beginning of each project in the form of a slide deck that covered all of the high level design decisions. This was a good way to get everyone on the same page, and go into the development process with a clear understanding of the project and associated risks, but it had a few drawbacks.

My biggest issue with this was that the document would often become out of date as the project progressed. Going back and updating a Powerpoint slide is an extra step that is easy to overlook when making a feature change, and would not be caught in the code review process. 

My solution was to introduce the team to `Sphinx` documentation, and incorporate it into our development process. Sphinx is a documentation generator that can use reStructuredText or Markdown as its markup language, and generate the documentation in a variety of formats (Docx, PDF, HTML, etc.). This provided a number of advantages over the previous process:

    - The documentation lives with the code, so it is convenient to update, and goes through reviews for each feature change.
    - The Formatting is programmatic. This means that formatting and style are consistent, and the developer can focus on the content.
    - Most software developers are more comfortable writing markdown than using tools like Powerpoint. 


.. note:: The document template is available on `Gitlab <https://gitlab.com/uprev/public/templates/sw-design-doc>`_


Devcontainers 
--------------

The next big change was the introduction of Development Containers. We often work on projects as a team and collaborate with outside resources such as a customers internal engineering team. This means that we need to be able to quickly onboard new developers, and provide a consistent development environment for everyone. By creating Docker containers which contain all of the necessary dependencies, and toolchains required for a project or platform we are able to ensure that everyone is working in the same environment. 

This also provided us with a good foundation for CI/CD since we can spin up a container for each build and know that it will be the same output as the developers local environment. As an added benefit we are able to deliver the development container as part of our deliverable package, which allows our customers to fully own their products and know that they will be able to build and maintain them in the future.

.. note:: All of our development containers for common platforms are available on `Dockerhub <https://hub.docker.com/u/uprev>_`. and source code is available on `Gitlab <https://gitlab.com/uprev/public/devcontainers>`_



Gitlab CI/CD
------------

Part of the revamp was also a change in tools. Previously we had used Bitbucket for our source control, and Jenkins for our CI/CD. I decided to move us to Gitlab for our source control and CI/CD. This was a big change but just like with documentation, I really felt that the if the CI script lived with the code it would be easier to maintain and keep up to date.

Because we develop solutions for a variety of customers (many of whome have multiple projects), Gitlabs 'Groups' feature was a great fit for our organizational needs. It allows us to create a group for each customer, and then create projects within that group for each of their products. This makes it cleaner to navigate and easier to manage access when working with the customers internal teams. 

The CI/CD features were also a big selling point for Gitlab. While a lot of source control services incorporate CI/CD, Gitlab seemed to do it with re-usabilitu in mind. We are able to create templates for commonly used platforms (STM32, nRF, Atmel, etc.) and then use those templates to quickly create a new pipeline for a project. This allows us to spend less time setting up the pipeline, and more time developing the product.

.. note:: The CI/CD templates are available on `Gitlab <https://gitlab.com/uprev/public/templates/ci-templates>`_

Lessons Learned 
---------------

One lesson learned was that there is no perfect solution for your specific needs. I spent a lot of time comparing different platforms and trying to find something that did exactly what I wanted out of the box. In the end I just had to pick something that would make the most sense in the long run, and do the work to make it fit our needs.
