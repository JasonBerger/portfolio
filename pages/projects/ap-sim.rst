AP Sim [2023]
=============

AP Sim (Advantage Player Simulator) was created as a tool to run simulations for different blackjack strategies, mainly testing profitability of various card counting techniques. It was mostly written to satisfy my own curiosity. I have seen the math on card counting, and trust the sources, but I wanted something that let me really experiment with different scenarios. I also wanted to see how seemingly small variations in rules could affect the overall odds of the game. 

originally I wrote a very simple python script that ran simulations and printed out a table of results. It was interesting, but I wanted to be able to run more complex scenarios, and I wanted to be able to visualize the results. 

I checked on `/r/blackjack <https://www.reddit.com/r/blackjack/>`_ and discovered that there is a tool for this called CVCX. It is a considered to be the gold standard for card counters, but it was a little expensive for my purposes, and has a very dated interface: 

.. image:: ../../assets/images/cvcx.png
   :align: center

|

I thought it would be fun to write my own tool for this, and make it more flexible and modern. The result is a simulator that is fully customizable and can run millions of rounds a minute. Users can alter basic strategy (when to hit/stand etc), tweak table rules, and edit bet spreads (bet size variations based on the count). Custom side bets and counting methods can also be scripted in Javascript and added to the simulation. 

.. image:: ../../assets/images/ap-sim.png
   :align: center

|

Live Demo 
---------

A demo of the tool is live at `ap-sim.com <http://ap-sim.com/>`_ and free for anyone to use. There are plans to add more features and allow users to save their simulations. 

By defaul there are 2 players configured 'AP' (short for Advantage Player) and 'Ploppy' a term used by advantage players to describe a casual gambler. AP is using a basic Hi-Lo counting strategy and bet spread, while Ploppy is using basic strategy, flat betting, and playing the 'lucky ladies' side bet. When you click 'Run' you can view the results of both approaches. 

.. warning:: There is currently a bug with some of the form validation, if a 'Save' or 'Accept' button is not working, you can press 'Enter' as a workaround.

Lessons Learned 
---------------

While the project was really meant to be a fun side project to curb my curiosity, It was also a pretty good learning experience. initially the performance of the simulation was pretty weak (tens of thousands of rounds a minute), so it was a fun challenge to optimize code and learn more about distributing work across multiple threads in javascript. 

During development I also struggled to find a good tree-view component for the UI. I ended up using one that had some style problems and lacked a few features I wanted, so I was able to contribute to the project with features and bug fixes. You can see those contributions here: https://github.com/geekhybrid/vue3-tree-vue 