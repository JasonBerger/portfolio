STM32 Touchscreen Platform [2013]
=================================

This project was actually related to my job at the time. My employer wanted a handheld datalogger for our products with a friendly touch screen interface. I was the only person at the company with an engineering role, so I was responsible for the full hardware and firmware design. This was by far the most complex project I had ever attempted, and I learned a lot in the process.

GUI Framework 
-------------

I was still a little inexperienced in this area and made the common mistake of re-inventing the wheel when it came to the GUI. I wrote a framework of UI elements and a simple event system to handle user input. I also wrote a simple graphics library to draw the UI elements and a desktop application (written in C#) that allowed the user to design the UI in XML .It would generate a preview, and output the C++ code for initializing the UI elements. 

Overall I would say this effort was successful, and I learned a lot in the process. However, if I had to accomplish the same task today, I would definitely take a different approach.  

.. youtube:: gpXB24gFGis
    :align: center

|
|
|


Hardware Design
---------------

The hardware design was based on the STM32F407 and was straightforward but had a lot of complexity due to the number of peripherals. The end product needed to be a customizable platform capable of interfacing with a wide variety of sensors and actuators, so the design was kept as modular as possible. Logging had to be supported on and SD card as well as USB flash drives. A bluetooth module was also incorporated to add wireless serial communication with a host machine so that logs could be retrieved even when the device was being used in an air-tight enclosure. KiCad was used for the Schematic capture and PCB layout.


Lessons Learned 
---------------

The obviuous lesson here was to not re-invent the wheel. But it was also a great lesson in the importance of mentorship. In hindsight, I was just too inexperienced to be the sole engineer developing a product. I had no real gauge for the magnitude of the project, and didnt know how much I didnt know. The project went significantly over budget and over schedule. At the time I took this as a failure, but as I have gained experience in product development, I realize that the original estimates were just unrealsitic. With the hardware and software involved, even if I had leveraged an existing GUI framework, this project would have been more suited for a team of 3-4 engineers to meet the schedule. 

