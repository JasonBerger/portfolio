Logic Analyzer [2017]
=====================

A lot of my projects come from wanting a better tool for a job. This is one of those projects. I wanted a logic analyzer that could not only read and decode signals on a bus, but actively manipulate them when needed. There are a few logic analyzers that allow you to set channels as outputs, but the overall architecture of these devices makes it difficult to reproduce serial data on a bus. What I am looking for is a polished 