Device Driver Utility [2020]
============================

A very common task in embedded systems is writing device drivers for other components in the system. This usually involves reading the data sheet to get information about the registers, and then writing the code that actually reads/writes the registers. In most cases the interface for the registers is either I2C or SPI, and there are a handful of common operations that need to be performed. I decided that a lot of this could be abstracted and a simple tool could be used to generate the code for the device driver.

The tool wast added to the `mrtutils` package in 2020 and follows a similar pattern to other utilities in the suite. The device is described is a YAML file and the tool generates the code for the device driver. The file specifies the registers on the device (name, address, size, description). If the registers contain multiple fields, that can also be specified in the file. 

Generic Operations 
------------------

The first step was to write a reusable module that provided a generic interface for accessing registers. It needed to support both interfaces (I2C and SPI), and provide enough customization that it would cover variations in protocol. 

.. note:: Source for that module can be found here: https://gitlab.com/uprev/public/mrt/modules/Devices/RegDevice

File Format 
-----------

The definition files are kept simple, but also offer a lot of flexibility. This is an example for the `HTS221` temperature and humidity sensor:

.. code:: yaml

    ---
    name: HTS221
    description: Humidity and Temperature Sensor
    category: Device
    requires: [RegDevice,Platform]
    datasheet: https://www.st.com/content/ccc/resource/technical/document/datasheet/4d/9a/9c/ad/25/07/42/34/DM00116291.pdf/files/DM00116291.pdf/jcr:content/translations/en.DM00116291.pdf
    mfr: STMicroelectronics
    mfr_pn: HTS221TR
    digikey_pn: 497-15382-1-ND

    prefix: HTS
    bus: I2C
    i2c_addr: 0xBE


    ###########################################################################################################
    #                                   Registers                                                             #
    ###########################################################################################################

    registers:
    - WHO_AM_I:     { addr: 0x0F , type: uint8_t, perm: R, desc:  Id Register, default: 0xBC}
    - AV_CONF:      { addr: 0x10 , type: uint8_t, perm: RW, desc: Humidity and temperature resolution mode}
    - CTRL1:        { addr: 0x20 , type: uint8_t, perm: RW, desc: Control register 1}
    - CTRL2:        { addr: 0x21 , type: uint8_t, perm: RW, desc: Control register 2}
    - CTRL3:        { addr: 0x22 , type: uint8_t, perm: RW, desc: Control register 3}
    - STATUS:       { addr: 0x27 , type: uint8_t, perm: R, desc: Status register}
    - HUMIDITY_OUT: { addr: 0x28 , type: int16_t, perm: R, desc: Relative humidity data }
    - TEMP_OUT:     { addr: 0x2A , type: int16_t, perm: R, desc: Temperature data}

    - H0_rH_x2:     { addr: 0x30 , type: uint8_t, perm: R, desc: Calibration data}
    - H1_rH_x2:     { addr: 0x31 , type: uint8_t, perm: R, desc: Calibration data}
    - T0_DEGC_x8:   { addr: 0x32 , type: uint8_t, perm: R, desc: Calibration data}
    - T1_DEGC_x8:   { addr: 0x33 , type: uint8_t, perm: R, desc: Calibration data}
    - T1T0_MSB:     { addr: 0x35 , type: uint8_t, perm: R, desc: Calibration data}
    - H0_T0_OUT:    { addr: 0x36 , type: int16_t, perm: R, desc: Calibration data}
    - H1_T0_OUT:    { addr: 0x3A , type: int16_t, perm: R, desc: Calibration data}
    - T0_OUT:       { addr: 0x3C , type: int16_t, perm: R, desc: Calibration data}
    - T1_OUT:       { addr: 0x3E , type: int16_t, perm: R, desc: Calibration data}

    ###########################################################################################################
    #                                   FIELDS                                                                #
    ###########################################################################################################
    fields:
    - STATUS:
        - TEMP_READY: { mask: 0x01, desc: indicates that a temperature reading is ready }
        - HUM_READY: { mask: 0x02, desc: indicates that a humidity reading is ready }

    - CTRL1:
        - PD: {mask: 0x80, desc: power down mode}
        - BDU: {mask: 0x04, desc: Block Data update. Prevents update until LSB of data is read}
        - ODR:
            mask: 0x03
            desc: Selects the Output rate for the sensor data
            vals:
            - ONESHOT: { val: 0, desc: readings must be requested}
            - 1HZ: { val: 1, desc: 1 hz sampling}
            - 7HZ: { val: 2, desc: 7 hz sampling}
            - 12_5HZ: { val: 3, desc: 12.5 hz sampling}

    - CTRL2:
        - BOOT: {mask: 0x80, desc: Reboot memory content}
        - HEATER: {mask: 0x02, desc: Enable intenal heating element}
        - ONESHOT: {mask: 0x01, desc: Start conversion for new data}

    - TEMP_OUT:
        - TEMP_OUT: {mask: 0xFFFF, desc: Current ADC reading for temperature sensor}

    - HUMIDITY_OUT:
        - HUM_OUT: {mask: 0xFFFF, desc: Current ADC reading for humidity sensor}

    ###########################################################################################################
    #                                   Preset Configs                                                        #
    ###########################################################################################################
    configs:
    - auto_1hz:
        desc: Sets device to update every second
        registers:
            - CTRL2: {BOOT: 1, delay: 20} #20 ms delay after register write
            - CTRL1: { ODR: 1HZ, BDU: 1}



`mrt-device` will take this file and generate the driver source which consists of 3 files: 

    - `hts221.h` - header file with function prototypes a struct for the device 
    - `hts221.c` - source file with function implementations
    - `hts221_regs.h` - header file with register definitions and macros for accessing fields

The generated code has `user-block` sections that allow the user to add custom code to the generated files. These sections are preserved if the file is regerenated, so the definition file can be updated if needed. 

Another good example of this can be found in the `WM8731 audio codec driver <https://gitlab.com/uprev/public/mrt/modules/Devices/Audio/WM8731>`_ . This device supports both I2C and SPI. It also shows a good use of the preset configurations. This section allows the user to define preset configurations that might be common use cases, and then a macro is generated that can be used to set all of the required registers. The registers are set in the order they are defined in the file, and a delay can be defined for each step if needed. 


Future Plans 
------------

Because the registers and fields are defined in a standard file, I plan to write a plugin for logic analyzers that would use this information along with bus traffic to show register values in real time. I think this could provide a lot of value when debugging device drivers.

More Information
----------------

For more information about the `mrt-device` tool see the `documentation <https://mrt.readthedocs.io/en/latest/pages/mrtutils/mrt-device.html>`_ .