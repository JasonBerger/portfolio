Color Tracking [2011]
=====================

The objective of this project was to learn more about image processing and objection detection. The idea was to track objects in a video stream by color. The approach was pretty basic but it was a good learning experience.


Basic Tracking 
--------------

This first video shows an early implementation where I was using my own color matching and filtering functions. I was using AForge.NET, but did not know enough at the time to leverage its capabilities. 

.. youtube:: upMNf-evGIs
    :align: center

|
|
|

Using Blobs 
-----------

As I learned more about the capabilities of AForge.NET, I switched over to using some of the built in filtering and blob detection, this really improved the reliability of the tracking.

.. youtube:: Qk6QPPlP4jo
    :align: center


|
|
|

Moving the Camera 
-----------------

At this point I decided it would be cool to have the camera move to follow the object. This would be my first introduction to embedded software using the Arduino. I built a turret style stand for the camera and connected it to two servos. Then I wrote a simple arduino sketch to take in serial data and move the servos accordingly.

The camera mounted on the turret was one I had removed the IR filter from, so for testing I used a TV remote to give it something to track. 

.. youtube:: pm7DUWTtUbQ
    :align: center


Lessons Learned 
---------------

This was a great experience to learn about computer vision, but it was also a bit of a turning point for me since it was my first introduction into embedded software and serial communication. It as very cool to see my code controlling a physical object, and definitely inspired me to learn more about embedded systems.

