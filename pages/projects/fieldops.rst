FieldOps [2023]
===============

Nobody ships a bug free product at launch, which is why almost every customer requests some OTA (Over The Air) update capabilities for their product. And while it is usually pretty simple to add an update protocol and bootloader to an embedded systen, most people dont consider the infastructure required to actually manage and deploy those updates. The development cost for a platform to manage OTA updates would be prohibitive for single product, which is why I created an open source solution called `FieldOps`.

The goal was a Dashboard and API flexible enough to support most products, but simple enough that it could be maintained and utilized by non developers. It needed to be secure, scalable, and easy to deploy.  It provides a number of features: 

    - Firmware/Package Management and deployment
    - Device Logging
    - Remote Device Configuration
    - Error Reporting


The system consists of a backend API (Node/Express), and a Dashboard (Vue3). There is also a python package and client library that can be used for automated deployments of update images. 

.. image:: ../../assets/images/platform_view.png 
   :align: center


FieldOps allows rules for each `platform` to determin which packages and versions are available to which devices. It also allows device and versions to be assigned to different deployment tracks (alpha, beta, release, etc) so that updates can be tested in a controlled setting and verified before being released to all devices in the field. 

Live Demo 
---------

A live demo of the Dashboard is available at `test.field-ops.io <http://test.field-ops.io/>`_ 

    - User: test@field-ops.io 
    - Password: testpassword 


.. note:: For more detailed documentation see the `docs <https://fieldops.readthedocs.io/en/latest/index.html>`_ 

Lessons Learned 
---------------

I mainly focus on embedded software, so I am very familiar with REST APIs as a client, but this was my first time building a proper REST API. I learned a lot about the best practices for REST APIs, and how to properly structure and document them. Part of the drive behind this project was also wanting to get familiar with some technologies/frameworks that I hear about often, but have never used personally. In this case those were Express and MongoDB.

One design decision that I now question was making it single-tenant. I did this because I wanted to make it as simple as possible to deploy, and secure. But now I realize that some of our less-technical clients would really like this available as a SaaS product. I would like to add multi-tenancy in the future, but it would have been a less painful process if I had designed it that way from the start.