PolyPacket C Library 
====================

`PolyPacket <https://mrt.readthedocs.io/en/latest/pages/polypacket/polypacket.html>`_ is a utility and backend library for quickly developing and implementing custom serial communication protocols for embedded systems. The code required solid memory management, good error handling, unit testing, and extensive documentation. I think it is a good example of my coding style and ability to write clean, well-documented, and well-tested code.


The library was written as a modular system leting me start with the smallest parts and build them up together to create the overall system. 


poly_field 
----------

Fields are the smallest unit of data, they represent a single value within a packet. They are defined by the user with a name, and type which includes a size in the case of arrays or strings. 


poly_packet
-----------

Packets consists of a collection of fields, and meta data (length, checksum, sequence number, and token). 

poly_service 
------------

The service is the engine that is responsible for parsing, building, and dispatching packets. Because this is the main interface for the developer I wanted to keep it simple but flexible. There is a `poly_service_feed` function that the user can pass raw bytes as they come in from a serial interface. The service will automatically parse the bytes into packets and dispatch them to handlers as needed. For outgoing packets the user can register a `bytes_tx` callback or a `packet_tx` callback which will either send raw bytes or a packet respectively.

.. In most use cases, there are 4 simple steps to intergrating a service with the user's application:

.. 1. Call the generated `xx_service_feed` with raw bytes as they come in from the serial interface
.. 2. Register a `bytes_tx` callback to send raw bytes out on the serial interface
.. 3. Call the generated `xx_service_process` function periodically to process packets and dispatch them to handlers
.. 4. Fill out the generated packet handler functions to handle incoming packets and build repsonses  

poly_spool 
----------

The spool is the outgoing queue for packets. It holds the outgoing packet in memory until one of the following events occurs:
    
    - The packet is acknowledged
    - The packet times out (no ack received, and maximum retries reached)



View the full source code here: `Utilities/PolyPacket <https://gitlab.com/uprev/public/mrt/modules/Utilities/PolyPacket>`_
