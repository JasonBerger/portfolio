NRF5 Example Project 
====================

The `NRF5 Gatt example project <https://gitlab.com/uprev/public/examples/nrf5-gatt-example>`_ was originally created for a demo and tutorial of the `mrt-ble` tool which is used to generate GATT profiles for BLE devices. Over time it has been used to demonstrate other capabilities for customers. 

The main reasons I am including this project is because I think it demonstrates well structured documentation and a robust CI/CD pipeline. 

Documentation
-------------

The project contains a `README.rst` file in the root directory that includes all of the information needed to build the project. This is a great practice because projects change hands over their lifetime, and it is important for a new developer to be able to get up and running quickly.

There is also a `doc` subdirectory which contains a sphinx project that builds out the full design documentation. The documentation can be built as an html page, or a pdf file. I have spent a lot of my career working in the engineering services industry and have worked with a lot of customers that brought us poorly documented projects. This experience has taught me the value of good documentation, and what information is important to capture. By documenting key design decisions, the project can be maintained and extended by other developers.

The CI/CD pipeline is configured to build the documentation and publish it using Gitlab Pages: `NRF5 Gatt Example Project Documentation <https://uprev.gitlab.io/public/examples/nrf5-gatt-example/>`_

.. warning:: The documentation was created as a demonstration and some of the features documented (such as OTA and bootloader) are not implemented in the project code. 


CI/CD Pipeline
--------------

The CI/CD pipeline for the project performs a number of tasks: 
    
    - Updates the version.h header using the `mrt-version <https://mrt.readthedocs.io/en/latest/pages/mrtutils/mrt-version.html#mrt-version-page>`_ tool 
    - Builds the project firmware binaries 
    - Builds the project documentation (PDF) 
    - Builds and publishes the HTML version of the documentation using Gitlab Pages
    - Deploys the latest firmware binary to `FieldOps <http://test.field-ops.io>`_

The pipeline makes use of the Gitlab 'Include' feature. This allows job templates to be defined and reused for multiple projects. By templatin common tasks, the pipelines can be kept simple and easy to maintain while still providing a lot of functionality. 

View the full code here: `NRF5 Gatt Example project <https://gitlab.com/uprev/public/examples/nrf5-gatt-example>`_



