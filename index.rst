Jason Berger
============

This is a portfolio of various work throughout my time in software and emebedded systems. I had an interest in electronics long before it became a career, so some of the earlier work is more hobbyist in nature, but I think it still shows my passion for the field. I am not able to publicy post details about a lot of the projects I have worked on professionally, but I can discuss more specific projects and experience in person. 


.. toctree::
    :maxdepth: 1
    :hidden:
    :caption: Resume
    
    pages/resume

.. toctree::
    :maxdepth: 1
    :caption: Projects
    
    pages/projects/ap-sim
    pages/projects/fieldops
    pages/projects/devops 
    pages/projects/mrt-ble
    pages/projects/mrt-device
    pages/projects/edge-connect
    pages/projects/polypacket
    pages/projects/nrf-research
    pages/projects/mrt
    pages/projects/card-detection
    pages/projects/stm32-gui
    pages/projects/quadbot
    pages/projects/color-tracking

.. toctree::
    :titlesonly:
    :caption: Code Samples 
    
    pages/code/nrf-example
    pages/code/cobs 
    pages/code/poly
    pages/code/driver

